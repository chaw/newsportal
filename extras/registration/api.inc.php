<?

/*
 * Does a user have read access on a newsgroup?
 *
 * @param String $groupname  Name of the newsgroup, "amrhein.test" for example
 * @param Int uid  User ID of the user
 *
 * @return true if the user has access, false if not
 */
function npreg_group_has_read_access($groupname,$uid=false) {
  return true;
}

/*
 * Does a user have write access on a newsgroup?
 *
 * @param String $groupname  Name of the newsgroup, "amrhein.test" for example
 * @param Int uid  User ID of the user
 *
 * @return true if the user has access, false if not
 */
function npreg_group_has_write_access($groupname,$uid=false) {
  return true;
}

/*
 * OPTIONAL: is the user moderator in the group, so he is allowed to cancel articles?
 *
 * @param String $groupname  Name of the newsgroup, "amrhein.test" for example
 * @param Int uid  User ID of the user
 * @return true if the logged in user is moderator or owner of the board or admin, otherwise false
 *
 * Be careful if your newsportal gives access to a public newsgroups network, for example 
 * the usenet: make sure, you or your moderators have the right to cancel articles. In the
 * usenet this is normaly not allowed!
 */
function npreg_user_is_moderator($groupname,$uid=false) {
  return false;
}

/*
 * name of the user. this function will fetch the user information from a session
 * variable or another external source. the name is used for post.php, to automatic
 * fill out the name-field in the form.
 *
 * @return the name of the user, or false if nobody is logged in
 */
function npreg_get_name() {
  return false;
}

/*
 * the users email-address. this function will fetch the user information from a session
 * variable or another external source.
 *
 * @return the email address of the user, or false if nobody is logged in or the user doesn't want to publish his email-address
 */
function npreg_get_email() {
  return false;
}


?>